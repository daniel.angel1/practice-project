<p align="center"><a href="https://elaniin.com/" target="_blank"><img src="https://lists.greatplacetowork.net/images/CAC/Elaniin.png" width="400"></a></p>


## About Practice project

Test project with laravel

## Installation method

#### 1-Clone project
```
    git clone https://gitlab.com/daniel.angel1/practice-project.git
```
#### 2-Update project
``` 
    cd practice-project
    npm install
```
#### 3-Update env
***
* Update  the email credentials

[![Screenshot-4.png](https://i.postimg.cc/Fs0X6hzM/Screenshot-4.png)](https://postimg.cc/w7xry8h2)

* Update  the database credentials

[![Screenshot-5.png](https://i.postimg.cc/xdMmJJ1s/Screenshot-5.png)](https://postimg.cc/KKcjCYLL)

* Update  the secret key for jwt or generate a new

[![Screenshot-6.png](https://i.postimg.cc/G2MmHdG7/Screenshot-6.png)](https://postimg.cc/vgVsKRPf)

##### Generate a new Jwt secret key
```
php artisan php artisan jwt:secret
```
***
## Migration .

#### 4-Migrate database
```injectablephp
    php artisan migrate
```


#### 5-Run seeders
``` 
    php artisan db:seed
```

###### __with this we would already have test data in our database__

#### 6-Run project
``` 
    php artisan serve
```
***

## Links.

#### Url hosted on the server for request with postman
```
    https://pure-hamlet-71935.herokuapp.com/api
```

####  Url hosted on the server documentation api
```
    http://pure-hamlet-71935.herokuapp.com/api/documentation
```

## Code of Conduct

In order to ensure that the Laravel community is welcoming to all, please review and abide by the [Code of Conduct](https://laravel.com/docs/contributions#code-of-conduct).

## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell via [taylor@laravel.com](mailto:taylor@laravel.com). All security vulnerabilities will be promptly addressed.

## License

The Laravel framework is open-sourced software licensed under the MIT license.
