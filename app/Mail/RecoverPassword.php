<?php

namespace App\Mail;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class RecoverPassword extends Mailable
{
    use Queueable, SerializesModels;

    protected $user;
    protected $code;
    protected $timeExpiration;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, string $code, int $timeExpiration)
    {
        $this->user = $user;
        $this->code = $code;
        $this->timeExpiration = $timeExpiration;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mails.reset-password')
            ->with('user', $this->user)
            ->with('code', $this->code)
            ->with('timeExpiration', $this->timeExpiration)
            ->subject('Password recovery email | Practice project');
    }
}
