<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Tymon\JWTAuth\Exceptions\JWTException;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = null;
        try {
            $id = \Tymon\JWTAuth\Facades\JWTAuth::parseToken()->authenticate()->id;
        } catch (JWTException $e) {
        }

        return [
            'name' => 'required|string|min:8',
            'email' => 'required|string|min:8|unique:users' . ($id ? ",id,$id" : ''),
            'phone_number' => 'required|integer|min:8',
            'username' => 'required|string|min:8|unique:users' . ($id ? ",id,$id" : ''),
            'date_birth' => 'required|string',
            'password' => ($id ? 'string' : 'required|string|min:8|confirmed'),

        ];
    }
}
