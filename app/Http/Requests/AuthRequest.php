<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * @OA\Schema(
 *      title="Auth request",
 *      description="Store Project request body data",
 *      type="object"
 * )
 */
class AuthRequest extends FormRequest
{
    /**
     * @OA\Property(
     *      property="email",
     *      description="Description of the new project",
     *      example="This is new project's description"
     * )
     *
     * @var string
     */
    public $email;
    /**
     * @OA\Property(
     *      property="password",
     *      description="Description of the new project",
     *      example="This is new project's description"
     * )
     *
     * @var string
     */
    public $password;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email'=>'required|string',
            'password'=>'required|string|confirmed|min:8',
        ];
    }
}
