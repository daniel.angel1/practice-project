<?php

namespace App\Http\Controllers;

use App\Http\Requests\PictureRequest;
use App\Http\Requests\ProductRequest;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Tymon\JWTAuth\Facades\JWTAuth;

class ProductController extends Controller
{
    /**
     * @OA\Post(
     *      path="/products/upload/picture",
     *      security={{ "apiAuth": {} }},
     *      tags={"Products"},
     *      operationId="upload image product",
     *      summary="upload image product",
     *      description="",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation"
     *       ),
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *               required={"sku","picture"},
     *               @OA\Property(property="sku", type="string", format="text", example="sku the product"),
     *               @OA\Property(property="picture", type="string", format="text", example="the image in base64")
     *              ),
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *     )
     */
    public function uploadImageProduct(PictureRequest $request)
    {
        $product = $this->getProduct($request->sku);
        $image_path = public_path() . $product->image;
        if (File::exists($image_path) == true) {
            File::delete($image_path);
        }
        $url = $this->convertBase64ToImg($request->picture);
        $product->image = $url;
        $product->save();
        return response()->json(['message' => "Product image uploaded successfully."]);
    }

    /**
     * @OA\Post(
     *      path="/products",
     *      security={{ "apiAuth": {} }},
     *     tags={"Products"},
     *      operationId="create a new product",
     *      summary="create a new product",
     *      description="",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation"
     *       ),
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *               required={"name","amount","price","description"},
     *               @OA\Property(property="name", type="string", format="text", example="name the product"),
     *               @OA\Property(property="amount", type="integer", format="text", example="amount the product"),
     *               @OA\Property(property="price", type="numeric", format="text", example="200.50"),
     *               @OA\Property(property="description", type="string", format="text", example="descriptionn the product"),
     *              ),
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *     )
     */
    public function createProduct(ProductRequest $productRequest): \Illuminate\Http\JsonResponse
    {
        $user = JWTAuth::parseToken()->authenticate();
        Product::create(['id_user' => $user->id, 'sku' => Str::random(10)] + $productRequest->validated());
        return response()->json(['message' => "Product created successfully."]);
    }

    /**
     * @OA\Put(
     *      path="/products",
     *      security={{ "apiAuth": {} }},
     *     tags={"Products"},
     *      operationId="update product",
     *      summary="update product",
     *      description="",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation"
     *       ),
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *               required={"sku", "name","amount","price","description"},
     *               @OA\Property(property="sku", type="string", format="text", example="sku the product"),
     *               @OA\Property(property="name", type="string", format="text", example="name the product"),
     *               @OA\Property(property="amount", type="integer", format="text", example="100"),
     *               @OA\Property(property="price", type="numeric", format="text", example="200.50"),
     *               @OA\Property(property="description", type="string", format="text", example="descriptionn the product"),
     *              ),
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *     )
     */
    public function updateProduct(ProductRequest $productRequest): \Illuminate\Http\JsonResponse
    {
        $product = $this->getProduct($productRequest->sku);
        $product->name = $productRequest->name;
        $product->amount = $productRequest->amount;
        $product->price = $productRequest->price;
        $product->save();
        return response()->json(['message' => "Product updated successfully."]);
    }

    /**
     * @OA\Delete(
     *      path="/products",
     *      tags={"Products"},
     *      security={{ "apiAuth": {} }},
     *      operationId="delete product",
     *      summary="delete product",
     *      description="",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation"
     *       ),
     *      @OA\Parameter(
     *          required=true,
     *          name="sku",
     *          in="path"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *     )
     */
    public function deleteProduct(Request $request): \Illuminate\Http\JsonResponse
    {
        $sku = $request->get('sku');
        $product = $this->getProduct($sku);
        $product->delete();
        return response()->json(['message' => "Product deleted successfully."]);
    }

    protected function getProduct($sku)
    {
        return Product::where('sku', '=', $sku)->first();
    }

    /**
     * @OA\Get (
     *      path="/products",
     *      security={{ "apiAuth": {} }},
     *      tags={"Products"},
     *      operationId="find  product",
     *      summary="find product",
     *      description="",
     *      @OA\Parameter(
     *          required=true,
     *          name="search",
     *          in="query",
     *          @OA\Schema(
     *          type="string"
     *          )
     *      ),
     *      @OA\Parameter(
     *          required=true,
     *          name="page",
     *          in="query",
     *          @OA\Schema(
     *          type="integer",
     *          format="int64"
     *          )
     *      ),
     *      @OA\Parameter(
     *          required=true,
     *          name="size",
     *          in="query",
     *          @OA\Schema(
     *          type="integer",
     *          format="int64"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation"
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *     )
     */
    public function findProduct(Request $request): \Illuminate\Http\JsonResponse
    {
        Validator::validate($request->all(), [
            'search' => 'required|string',
            'page' => 'required|integer',
            'size' => 'required|integer'
        ]);
        $search = $request->query('search');
        $products = Product::query()
            ->where('name', 'LIKE', "%{$search}%")
            ->orWhere('sku', 'LIKE', "%{$search}%")
            ->paginate($request->get('size'), '*', 'page', $request->get('page'));
        return response()->json(['message' => "Operation successfully.", 'response' => $products]);
    }

    protected function convertBase64ToImg($imgBase64): string
    {
        preg_match("/data:image\/(.*?);/", $imgBase64, $image_extension); // extract the image extension
        $image = preg_replace('/data:image\/(.*?);base64,/', '', $imgBase64); // remove the type part
        $image = str_replace(' ', '+', $image);
        $file = base64_decode($image);
        $type = $image_extension[1];
        $safeName = time() . '.' . $type;
        $url = '/uploads/image_' . $safeName;
        file_put_contents(public_path() . $url, $file);
        return $url;
    }
}
