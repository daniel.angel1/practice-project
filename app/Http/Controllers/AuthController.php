<?php

namespace App\Http\Controllers;

use App\Http\Requests\AuthRequest;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;

class AuthController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * @OA\Post(
     *      path="/auth/login",
     *     tags={"Auth"},
     *      operationId="Login user",
     *      summary="login user",
     *      description="Returns token",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation"
     *       ),
     *      @OA\RequestBody(
     *          required=true,
     *          description="Pass user credentials",
     *          @OA\JsonContent(
     *               required={"email","password"},
     *               @OA\Property(property="email", type="string", format="email", example="user1@mail.com"),
     *               @OA\Property(property="password", type="string", format="password", example="PassWord12345"),
     *               @OA\Property(property="password_confirmation", type="string", format="password", example="PassWord12345"),
     *              ),
     *          ),
     *
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *     )
     */
    public function login(AuthRequest $request)
    {
        $credentials = $request->only('email', 'password');
        if (!$token = JWTAuth::attempt($credentials)) {
            return response()->json(['error' => 'invalid_credentials'], 400);
        }
        return $this->respondWithToken($token);
    }

    /**
     * @OA\Get(
     *      path="/auth/me",
     *      security={{ "apiAuth": {} }},
     *      tags={"Auth"},
     *      operationId="Get autenticate user",
     *      summary="Get autenticate user",
     *      description="Get autenticate user",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation"
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *     )
     */
    public function me()
    {
        return response()->json(JWTAuth::parseToken()->authenticate());
    }

    /**
     * @OA\Post(
     *      path="/auth/logout",
     *      security={{ "apiAuth": {} }},
     *     tags={"Auth"},
     *      operationId="Log the user out (Invalidate the token)",
     *      summary="Log the user out (Invalidate the token)",
     *      description="Log the user out (Invalidate the token)",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation"
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *     )
     */
    public function logout(Request $request)
    {
        JWTAuth::invalidate($request->get('Authorization'));
        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * @OA\Post(
     *      path="/auth/refresh",
     *      security={{ "apiAuth": {} }},
     *     tags={"Auth"},
     *      operationId="Refresh a token user",
     *      summary="Refresh a token user",
     *      description="Get autenticate user",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation"
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *     )
     */
    public function refresh(Request $request)
    {
        return $this->respondWithToken(JWTAuth::refresh($request->get('Authorization')));
    }

    /**
     * Get the token array structure.
     *
     * @param string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token): \Illuminate\Http\JsonResponse
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => JWTAuth::factory()->getTTL()
        ]);
    }

}
