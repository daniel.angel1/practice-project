<?php

namespace App\Http\Controllers;

use App\Exceptions\CodeException;
use App\Http\Requests\CodeRequest;
use App\Http\Requests\UserRequest;
use App\Http\Resources\UserResponse;
use App\Mail\RecoverPassword;
use App\Models\Code;
use App\Models\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class UserController extends Controller
{
    protected $successfully = 'Operation successfully';

    protected $user;
    protected $code;

    public function __construct(User $user, Code $code)
    {
        $this->user = $user;
        $this->code = $code;
    }

    /**
     * @OA\Post(
     *      path="/users",
     *      operationId="Register user",
     *      tags={"Users"},
     *      summary="Register user",
     *      description="Returns partial data info the user",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation"
     *       ),
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *               required={"name", "email", "password","phone_number","username","date_birth"},
     *               @OA\Property(property="name", type="string", format="text", example="your name"),
     *               @OA\Property(property="username", type="string", format="text", example="your username"),
     *               @OA\Property(property="date_birth", type="string", format="text", example="2013-05-01 15:00"),
     *               @OA\Property(property="phone_number", type="integer", format="text", example="77778888"),
     *               @OA\Property(property="email", type="string", format="email", example="user1@mail.com"),
     *               @OA\Property(property="password", type="string", format="password", example="PassWord12345"),
     *               @OA\Property(property="password_confirmation", type="string", format="password", example="PassWord12345"),
     *              ),
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *     )
     */
    public function register(UserRequest $userRequest)
    {
        $dateBirth = date("Y-m-d H:i:s", strtotime($userRequest->date_birth));
        $user = $this->user->create(['password' => bcrypt($userRequest->password),
            'date_birth' => $dateBirth,
            'name' => $userRequest->name,
            'email' => $userRequest->email,
            'username' => $userRequest->username,
            'phone_number' => $userRequest->phone_number,
        ]);
        return response()->json(['message' => $this->successfully, 'response' => UserResponse::make($user)], 200);
    }

    /**
     * @OA\Put(
     *      path="/users",
     *      tags={"Users"},
     *      security={{ "apiAuth": {} }},
     *      operationId="Update user",
     *      summary="Update user",
     *      description="Returns the user updated",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation"
     *       ),
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *               required={"name","id", "email", "password","phone_number","username","date_birth"},
     *               @OA\Property(property="id", type="integer", format="text", example="1"),
     *               @OA\Property(property="name", type="string", format="text", example="your name"),
     *               @OA\Property(property="username", type="string", format="text", example="your username"),
     *               @OA\Property(property="date_birth", type="string", format="text", example="2013-05-01 15:00"),
     *               @OA\Property(property="phone_number", type="integer", format="text", example="77778888"),
     *               @OA\Property(property="email", type="string", format="email", example="user1@mail.com"),
     *               @OA\Property(property="password", type="string", format="password", example="PassWord12345"),
     *               @OA\Property(property="password_confirmation", type="string", format="password", example="PassWord12345"),
     *              ),
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *     )
     */
    public function updateUser(UserRequest $userRequest)
    {
        $user = $this->user->findOrFail($userRequest->id);
        $user->fill($userRequest->all());
        $user->date_birth = date("Y-m-d H:i:s", strtotime($userRequest->date_birth));
        $user->save();
        return response()->json(['message' => $this->successfully, 'response' => UserResponse::make($user)], 200);
    }

    /**
     * @OA\Get(
     *      path="/users",
     *      tags={"Users"},
     *      security={{ "apiAuth": {} }},
     *      operationId="Find all users in paginate data",
     *      summary="find users",
     *      description="Returns Paginate data user",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation"
     *       ),
     *      @OA\Parameter(
     *          required=true,
     *          name="page",
     *          in="query",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Parameter(
     *          required=true,
     *          name="size",
     *          in="query",
     *          @OA\Schema(
     *           type="integer",
     *           format="int64"
     *          )
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *     )
     */
    public function findUser(Request $request): \Illuminate\Http\JsonResponse
    {
        Validator::validate($request->all(), [
            'page' => 'required|integer',
            'size' => 'required|integer'
        ]);
        $users = $this->user->query()->paginate($request->get('size'), '*', 'page', $request->get('page'));
        return response()->json(['message' => $this->successfully, 'response' => $users]);
    }

    /**
     * @OA\Post(
     *      path="/auth/recovery/password",
     *      tags={"Auth"},
     *      operationId="recovery user",
     *      summary="recovery user",
     *      description="",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation"
     *       ),
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *               required={"name", "email", "password","phone_number","username","date_birth"},
     *               @OA\Property(property="name", type="string", format="text", example="your name"),
     *               @OA\Property(property="username", type="string", format="text", example="your username"),
     *               @OA\Property(property="date_birth", type="string", format="text", example="2013-05-01 15:00"),
     *               @OA\Property(property="phone_number", type="integer", format="text", example="77778888"),
     *               @OA\Property(property="email", type="string", format="email", example="user1@mail.com"),
     *               @OA\Property(property="password", type="string", format="password", example="PassWord12345"),
     *               @OA\Property(property="password_confirmation", type="string", format="password", example="PassWord12345"),
     *              ),
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *     )
     */
    public function recoverPassword(Request $request)
    {
        Validator::validate($request->all(), [
            'email' => 'required|min:8|string|email'
        ]);

        $user = $this->user->where("email", "=", $request->get("email"))->first();
        $code = Str::random(16);
        $this->saveCode($user->id, $code);
        $this->sendMail($user, $code);
        return response()->json(['message' => 'The recovery email was sent successfully'], 200);
    }

    /**
     * @param $idUser
     * @param $code
     */
    protected function saveCode($idUser, $code)
    {
        $date = Carbon::now('GMT-6');
        $env = env('CODE_EXPIRATION');
        $expirationDate = $date->addMinutes($env);
        $this->code->create([
            'code' => $code,
            'id_user' => $idUser,
            'expiration' => $expirationDate
        ]);
    }

    /**
     * @param $user
     * @param $code
     */
    protected function sendMail($user, $code)
    {
        try {
            Mail::to($user->email)->send(new RecoverPassword($user, $code, env('CODE_EXPIRATION')));
        } catch (Exception $e) {
            error_log('Ocurrio un error al enviar el correo: ' . $e->getMessage());
        }
    }

    /**
     * @OA\Put(
     *      path="/auth/recovery/password",
     *      tags={"Auth"},
     *      operationId="update the password user",
     *      summary="update the password user",
     *      description="",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation"
     *       ),
     *      @OA\RequestBody(
     *          required=true
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *     )
     */
    public function updatePassword(CodeRequest $request)
    {
        $code = $this->code->where('code', '=', $request->code)->first();
        if ($code == null) {
            throw new CodeException("The verification code entered is not valid");
        } else if ($this->codeIsExpired($code)) {
            throw new CodeException("Verification code has expired");
        }
        $user = $code->user();
        if ($request->email == $user->email) {
            $user->password = bcrypt($request->password);
            $user->save();
            return response()->json(['message' => "The password was updated correctly"]);
        } else {
            throw new CodeException("The verification code entered is not valid");
        }
    }

    public function codeIsExpired($code)
    {
        $date = Carbon::now('GMT-6');
        $expirationDate = new Carbon($code->expiration);
        if ($date->toDateString() == $expirationDate->toDateString()
            && $date->toTimeString() <= $expirationDate->toTimeString()) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * @OA\Delete(
     *      path="/users",
     *      tags={"Users"},
     *      security={{ "apiAuth": {} }},
     *      operationId="delete the user",
     *      summary="delete the user",
     *      description="",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation"
     *       ),
     *      @OA\Parameter(
     *       description="ID of user",
     *       in="path",
     *       name="id",
     *       required=true,
     *       example="1",
     *       @OA\Schema(
     *          type="integer",
     *          format="int64"
     *          )
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *     )
     */
    public function deleteUser(Request $request)
    {
        User::delete($request->get('id'));
        return response()->json(['message' => "The user was deleted correctly"]);
    }
}
