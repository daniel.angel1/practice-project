<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group([
    'middleware' => 'api',
    'namespace' => 'App\Http\Controllers',
    'prefix' => 'auth'
], function ($router) {
    Route::post('/login', [AuthController::class, 'login']);
    Route::post('/recovery/password', [UserController::class, 'recoverPassword']);
    Route::put('/recovery/password', [UserController::class, 'updatePassword']);
});

Route::group([
    'middleware' => 'jwt.verify',
    'namespace' => 'App\Http\Controllers',
    'prefix' => 'auth'
], function ($router) {
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::get('me', 'AuthController@me');
});

Route::group([
    'middleware' => 'jwt.verify',
    'namespace' => 'App\Http\Controllers',
    'prefix' => 'products'
], function ($router) {
    Route::post('/', [ProductController::class, 'createProduct']);
    Route::put('/', [ProductController::class, 'updateProduct']);
    Route::delete('/', [ProductController::class, 'deleteProduct']);
    Route::put('/upload/picture', [ProductController::class, 'uploadImageProduct']);
    Route::get('/', [ProductController::class, 'findProduct']);
});

Route::post('/users', [UserController::class, 'register']);

Route::group([
    'middleware' => 'jwt.verify',
    'namespace' => 'App\Http\Controllers',
    'prefix' => 'users'
], function ($router) {
    Route::put('/', [UserController::class, 'updateUser']);
    Route::get('/', [UserController::class, 'findUser']);
    Route::delete('/', [UserController::class, 'deleteUser']);
});



