<?php

namespace Database\Factories;

use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class ProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name(),
            'sku' => Str::random(10),
            'amount' => $this->faker->numerify('####'),
            'price' => $this->faker->numerify('##.##'),
            'description' => $this->faker->text(),
            'id_user' => \App\Models\User::factory()
        ];
    }
}
